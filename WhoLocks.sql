SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[whoLocks] (@withDetails bit = 1)
as
BEGIN
    SET NOCOUNT ON
    DECLARE @who        TABLE
        (   spid        VARCHAR(15)
        ,   Status      VARCHAR(15)
        ,   loginname   SYSNAME
        ,   hostname    SYSNAME
        ,   blkBy       SYSNAME
        ,   dbname      SYSNAME  NULL
        ,   cmd         VARCHAR(MAX) NULL
        ,   CPUTime     INT
        ,   DiskIO      INT
        ,   LastBatch   VARCHAR(25)
        ,   ProgramName VARCHAR(255)
        ,   spid2       INT
        ,   requestId   INT
        ,   id          INT identity(1,1)
        ,   PRIMARY KEY CLUSTERED (spid, id)
        )

    DECLARE @locks   TABLE 
        (   spid     VARCHAR(15)
        ,   dbid     INT
        ,   objId    INT
        ,   IndId    INT
        ,   Type     VARCHAR(3)
        ,   Resource VARCHAR(50)
        ,   Mode     VARCHAR(15)
        ,   Status   VARCHAR(15)
        ,   id       INT IDENTITY
        ,   PRIMARY KEY CLUSTERED (spid, id)
        )
    INSERT INTO @who 
        (   spid      
        ,   Status    
        ,   loginname 
        ,   hostname  
        ,   blkBy     
        ,   dbname    
        ,   cmd       
        ,   CPUTime   
        ,   DiskIO    
        ,   LastBatch 
        ,   ProgramName
        ,   spid2     
        ,   requestId 
        )
    EXEC  sp_who2
    ;
    INSERT INTO @locks
        (   spid    
        ,   dbid    
        ,   objId   
        ,   IndId   
        ,   Type    
        ,   Resource
        ,   Mode    
        ,   Status  
        )
    EXEC  sp_lock
    ;
    SELECT distinct 
        [blocked.spid         ]=blocked.spid
    ,   [blocked.hostname     ]=convert(varchar(30), blocked.hostname      )
    ,   [blocked.program_name ]=convert(varchar(30), blocked.program_name  )
    ,   [blocked.cmd          ]=convert(varchar(30), blocked.cmd           )
    ,   [blocked.loginame     ]=convert(varchar(30), blocked.loginame      )
    ,   [blocking.spid        ]=blocking.spid
    ,   [blocking.hostname    ]=convert(varchar(30), blocking.hostname     )
    ,   [blocking.program_name]=convert(varchar(30), blocking.program_name )
    ,   [blocking.cmd         ]=convert(varchar(30), blocking.cmd          )
    ,   [blocking.loginame    ]=convert(varchar(30), blocking.loginame     )
    into #locks
    FROM @who who
    JOIN  sysprocesses blocked  on    blocked.spid =try_convert(int, who.spid )
    JOIN  sysprocesses blocking on    blocking.spid=try_convert(int, who.blkby)
    where isnumeric(who.blkby)=1
    and blocked.spid!=blocking.spid
    -- and     isnull(blocking.loginname, '') != ''
    -- ORDER BY blocked.blkBy
    ;
    if @@rowcount=0 print 'No session blocked'
    else begin
        select * from #locks
        -- exec sp_who
        select distinct  blocked.spid, blocked.waittime
        ,   [blocked.spid         ]=blocked.spid
        ,   [blocked.hostname     ]=convert(varchar(30), blocked.hostname      )
        ,   [blocked.program_name ]=convert(varchar(30), blocked.program_name  )
        ,   [blocked.cmd          ]=convert(varchar(30), blocked.cmd           )
        ,   [blocked.loginame     ]=convert(varchar(30), blocked.loginame      )
        ,   [blocking.spid        ]=blocking.spid
        ,   [blocking.hostname    ]=convert(varchar(30), blocking.hostname     )
        ,   [blocking.program_name]=convert(varchar(30), blocking.program_name )
        ,   [blocking.cmd         ]=convert(varchar(30), blocking.cmd          )
        ,   [blocking.loginame    ]=convert(varchar(30), blocking.loginame     )

        from sysprocesses blocked
        join syslockinfo wlock on wlock.req_spid=blocked.spid and wlock.req_status=3
        join syslockinfo block on block.rsc_bin=wlock.rsc_bin and block.req_status!=3
        join sysprocesses blocking on block.req_spid=blocking.spid

    end

    drop table #locks

    if @withDetails=1 begin
        SELECT [counts]=null, db=DB_NAME(DBID), l.spid, loginname, hostname, [count of locks]=count(*) 
        FROM @locks l
        JOIN (SELECT DISTINCT spid, loginname, hostname FROM @who WHERE isnull(loginname, '') != '') w 
        ON   l.spid=w.spid
        GROUP BY DB_NAME(DBID), l.spid, loginname, hostname
        ORDER BY count(*) DESC
        ;
        SELECT [detail]=null, db=DB_NAME(DBID), OBJECT=OBJECT_NAME(OBJID, DBID), * FROM @locks l
        JOIN @who w on l.spid=w.spid
        ;
    END;
END;

go
set rowcount 0
exec wholocks 0

exec sp_who2

select * from sysobjects where name like 'sys%'

select * from sysprocesses
where blocked!=0
select distinct  blocked.spid, blocked.waittime

,   [blocked.spid         ]=blocked.spid
,   [blocked.hostname     ]=convert(varchar(30), blocked.hostname      )
,   [blocked.program_name ]=convert(varchar(30), blocked.program_name  )
,   [blocked.cmd          ]=convert(varchar(30), blocked.cmd           )
,   [blocked.loginame     ]=convert(varchar(30), blocked.loginame      )
,   [blocking.spid        ]=blocking.spid
,   [blocking.hostname    ]=convert(varchar(30), blocking.hostname     )
,   [blocking.program_name]=convert(varchar(30), blocking.program_name )
,   [blocking.cmd         ]=convert(varchar(30), blocking.cmd          )
,   [blocking.loginame    ]=convert(varchar(30), blocking.loginame     )

from sysprocesses blocked
join syslockinfo wlock on wlock.req_spid=blocked.spid and wlock.req_status=3
join syslockinfo block on block.rsc_bin=wlock.rsc_bin and block.req_status!=3
join sysprocesses blocking on block.req_spid=blocking.spid



where blocked.blocked!=0