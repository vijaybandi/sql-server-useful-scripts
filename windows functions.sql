USE [CXLTESTS]
GO

WITH series as (select seq=value, price=RAND((1+SIN((value*99999) % 113))*1000)*50, seed=(1+SIN((value*99999) % 113))*1000 from  [dbo].[IntRange] (1,1,20)) 
, ma as (
    SELECT  seq, price, seed
    ,       ma10=avg(price) over  ( order by seq rows between 9 preceding and current row)
    ,       cma10=count(seq) over  ( order by seq rows between 9 preceding and current row)
    FROM  series
) 
select case cma10 when 10 then ma10 else ma10 end, 
* from ma
GO

-- select rand(1), rand(2), RAND(3), RAND(4000)
