USE [GlobalGasOptimization]
GO

/****** Object:  View [staging].[DstSwitches]    Script Date: 10/29/2014 9:58:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [staging].[DstSwitches] as 
/* http://en.wikipedia.org/wiki/Daylight_saving_time_by_country */
with years as (SELECT yy=value from dbo.intrange(0,1,20))
,    zones as (SELECT * from (values ('BTP'),('CET')) x(timezone))
,    mars  as (SELECT d=dateadd(dd, -1, dateadd(mm, 03, dateadd(yy, yy, '20090101'))), offset=-1 FROM years) 
,    octs  as (SELECT d=dateadd(dd, -1, dateadd(mm, 10, dateadd(yy, yy, '20090101'))), offset=+1 FROM years)
,    allds as (SELECT * FROM mars union SELECT * FROM octs)
,    sats  as (SELECT d=dateadd(hh, 2.5+offset/2.0, d - (datepart(dw, d)+ @@DATEFIRST-1) % 7), offset FROM allds)
SELECT timezone, DstSwitchTime=d, offset 
FROM sats
cross join zones


GO


select * from [staging].[DstSwitches]