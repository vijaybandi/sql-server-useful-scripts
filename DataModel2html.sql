SET NOCOUNT ON
GO

WITH tables as (
    SELECT Table_schema, Table_name
    FROM INFORMATION_SCHEMA.TABLES t
    WHERE table_name NOT LIKE '[_][_]%'
    AND   table_name NOT LIKE 'sys%'
)
,   RawPageWrapping as (
    SELECT * FROM (
        VALUES ('0', 1,'<HTML><head>')
        ,      ('0', 2,'<style>
table
{
    width: 100%;
    padding: 0px;
    border-spacing: 0;
}

td.column
{
    border-bottom: 1px green solid;
    border-right: 1px green solid;
    margin: 0;
    padding: 1px;
}

th.column {
    border-bottom: 1px darkblue solid;
    border-right: 1px darkblue solid;
    text-wrap: none;
    vertical-align: bottom;
}

th.table
{
    text-align: left;
    font-family: sans-serif;
    font-size: large;
    margin: 0;
    padding-top: 25px;
    padding-bottom: 15px;
}

td.table
{
    text-align: left;
    padding-bottom: 15px;
}

tr.table
{
    color: darkblue;
}

tr.column
{
    color: darkblue;
    padding: 0px;
    text-align: left;
    border-bottom: 1px darkblue solid;
    border-right: 1px darkblue solid;
    vertical-align: top;
}
th.database {
    font-size: x-large;
}
</style>')
    ,   ('0', 3, '<title>Data model: ' + db_name() + '</title>')
    ,   ('0', 4, '</head>')
    ,   ('0', 5, '<body>')
    ,   ('0', 6, '<h2 class="database">Data model: ' + db_name() + '</h2>')
    ,   ('0', 7, '<table>')
    ,   ('9', 1, '</table>')
    ,   ('9', 2, '</body>')
    ,   ('9', 3, '</html>')
) xx(section, seq, h)
)
,   tablesObj as (
    SELECT Table_name=table_schema + '.' + Table_name, st.object_id, TableDesc=exprop.value
    FROM tables t
    JOIN sys.tables st on st.name=t.table_name and schema_name(st.schema_id)=t.table_schema
    LEFT JOIN sys.extended_properties exprop 
           ON exprop.major_id = st.object_id
          AND exprop.minor_id = 0
          AND exprop.NAME = 'MS_Description'
    WHERE table_name NOT LIKE '[_][_]%'
    AND   table_name NOT LIKE 'sys%'
)
,   tableHeader AS (
        SELECT section='1.'+Table_name, seq=-2, h='<tr><th colspan="10" class="table"><h3>Table: '+ Table_name
            , q='</h3></th></tr>'
        FROM tablesObj
        UNION ALL
        SELECT section='1.'+Table_name, seq=-1, h='<tr><td colspan="10" class="table">'
            + REPLACE(CAST(ISNULL(TableDesc, '') AS VARCHAR(8000)), CHAR(13)+CHAR(10), '<br/>')
            , q='</td></tr>'
        FROM tablesObj
        where TableDesc IS NOT NULL
    )
,   columnHeader AS (
        SELECT section='1.'+Table_name, seq=0, h='<tr class="column"><th class="column">'
            , c01='Column Name'
            , c02='Description'
            , c03='In Primary Key'
            , c04='Is Foreign Key'
            , c05='Data Type'
            , c06='Length'
            , c07='Numeric Precision'
            , c08='Nullable'
            , c09='Computed'
            , c10='Identity'
            , c11='Default Value'
            , s='</th><th class="column">'
            , q='</th></tr>'
        FROM tablesObj
    )
,   columns AS (
 SELECT section='1.' + Table_name, seq=clmns.column_id
            , h= '<tr class="column"><td class="column">'
            , c01=clmns.NAME
            , c02=REPLACE(CAST(ISNULL(exprop.value, '') AS VARCHAR(8000)), CHAR(13)+CHAR(10), '<br/>')
            , c03=CASE WHEN idxcol.index_column_id=1 THEN 'yes' ELSE '' END
            , c04=ISNULL((
                    SELECT TOP 1 /*schema_name(referenced_object_id) +'.'+*/ object_name(constraint_object_id) + ' (' + OBJECT_SCHEMA_NAME(referenced_object_id) + '.' + object_name(referenced_object_id) + '.' + col_name(referenced_object_id, referenced_column_id) + ')'
                    FROM sys.foreign_key_columns AS fkclmn
                    WHERE fkclmn.parent_column_id = clmns.column_id
                        AND fkclmn.parent_object_id = clmns.object_id
                    ), '')
            , c05=udt.NAME
            , c06= CAST(CASE 
                    WHEN typ.NAME IN (N'nchar', N'nvarchar')
                        AND clmns.max_length <> - 1
                        THEN clmns.max_length / 2
                    ELSE clmns.max_length
                    END AS VARCHAR(20))
            , c07=IIF(clmns.precision  =0, '', CAST( clmns.precision -clmns.scale AS VARCHAR(20))+'.'+CAST( clmns.scale      AS VARCHAR(20)))
            , c08=IIF(clmns.is_nullable=0, '', 'yes')
            , c09=IIF(clmns.is_computed=0, '', 'yes')
            , c10=IIF(clmns.is_identity=0, '', 'yes')
            , c11=ISNULL(cnstr.DEFINITION, '')
            , s='</td><td class="column">'
            , q='</td></tr>'
    FROM tablesObj tbl
    JOIN sys.all_columns AS clmns ON clmns.object_id = tbl.object_id
    LEFT JOIN sys.indexes AS idx ON idx.object_id = clmns.object_id
        AND 1 = idx.is_primary_key
    LEFT JOIN sys.index_columns AS idxcol ON idxcol.index_id = idx.index_id
        AND idxcol.column_id = clmns.column_id
        AND idxcol.object_id = clmns.object_id
        AND 0 = idxcol.is_included_column
    LEFT JOIN sys.types AS udt ON udt.user_type_id = clmns.user_type_id
    LEFT JOIN sys.types AS typ ON typ.user_type_id = clmns.system_type_id
        AND typ.user_type_id = typ.system_type_id
    LEFT JOIN sys.default_constraints AS cnstr ON cnstr.object_id = clmns.default_object_id
    LEFT JOIN sys.extended_properties exprop ON exprop.major_id = clmns.object_id
        AND exprop.minor_id = clmns.column_id
        AND exprop.NAME = 'MS_Description'
)
SELECT h, c01, s, c02, s, c03, s, c04, s, c05, s, c06, s
        , c07, s, c08, s, c09, s, c10, s, c11
        , q
FROM (
     SELECT section, seq, h
         , c01='', c02='', c03='', c04='', c05='', c06=''
         , c07='', c08='', c09='', c10='', c11=''
         , s='', q=''
     FROM RawPageWrapping
     UNION ALL
     SELECT section, seq, h
         , c01='', c02='', c03='', c04='', c05='', c06=''
         , c07='', c08='', c09='', c10='', c11=''
         , s='', q
     FROM tableHeader
     UNION ALL
          SELECT section, seq, h
         , c01, c02, c03, c04, c05, c06
         , c07, c08, c09, c10, c11
         , s, q
    FROM columnHeader
     UNION ALL
     SELECT section, seq, h
         , c01, c02, c03, c04, c05, c06
         , c07, c08, c09, c10, c11
         , s, q
    FROM columns
) x
order by section, seq
