declare @facts table (id int identity, measure1 float, measure2 float, measure3 float)

insert into @facts(measure1, measure2, measure3)
values (1.1, 1.2, 1.3)
,      (2.1, 2.2, 2.3)

select * from @facts

select id, seq, measure, value
from @facts
cross apply (
    values (1, 'Measure 1', measure1)
    ,      (2, 'Measure 2', measure2)
    ,      (3, 'Measure 3', measure3)
    ) x(seq, measure, value)